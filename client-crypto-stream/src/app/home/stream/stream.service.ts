import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlConfig } from 'src/app/url-config';

// service handels our http calls
// UrlConfig stores our url that we call
@Injectable()
export class StreamService {
    constructor(
        private http: HttpClient,
        private config: UrlConfig) { }

    async getUser() {
        return await this.http.get(this.config.user, {responseType: 'text'});
    }
    async getBTC() {
        return await this.http.get(this.config.streamBTC);
    }
    async getETH() {
        return await this.http.get(this.config.streamETH);
    }
    async getXRP() {
        return await this.http.get(this.config.streamXRP);
    }
    async getLTC() {
        return await this.http.get(this.config.streamLTC);
    }
    async getBCH() {
        return await this.http.get(this.config.streamBCH);
    }
    async getETC() {
        return await this.http.get(this.config.streamETC);
    }
}