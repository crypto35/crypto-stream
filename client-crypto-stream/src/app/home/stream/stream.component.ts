import { Component, OnInit } from '@angular/core';
import { StreamService } from './stream.service';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { io } from 'socket.io-client';


@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.scss']
})
export class StreamComponent implements OnInit {

  // declear various crypto currencies
  public BTC: any
  public ETH: any
  public XRP: any
  public LTC: any
  public BCH: any
  public ETC: any

  // user that "loged in"
  public user: string = ''

  // datasource for our return values from api
  public cur: any = {}

  constructor(
    private service: StreamService
  ) { }

  // socket we listen on
  socket = io('ws://localhost:3000');
  ngOnInit(): void {

    //get the loged in user
    this.getUser()

    // Listen to websocket event
    this.socket.on('msgToClient', data => {
      //build json object from the keys
      data.forEach((element: any) => {
        this.cur[`${Object.keys(element)}`] = element[`${Object.keys(element)}`]
      });
      // assign the rows
      this.BTC = this.cur.BTC
      this.ETH = this.cur.ETH
      this.XRP = this.cur.XRP
      this.LTC = this.cur.LTC
      this.BCH = this.cur.BCH
      this.ETC = this.cur.ETC
    })

    // emit so server can start stream data to us
    this.socket.emit('msgToServe', '123')
    setInterval(() =>{ this.send() }, 3000);
  }

  send() {
    this.socket.emit('msgToServe', '123')
  }

  // create array form json
  getKeys(data: any) {
    let keys = [];
    for (let key in data) {
      keys.push({ key: key, value: data[key] });
    }
    return keys
  }

  // for manual call of crypto - no longer used
  getCrypto(): void {
    this.getBTC();
    this.getETH();
    this.getXRP();
    this.getLTC();
    this.getBCH();
    this.getETC();
  }

  // call to retrieve the user
  async getUser(): Promise<void> {
    (await this.service.getUser()).subscribe((response: any) => {
      console.log(response)
      this.user = response
    })
  }

  // various calls for each crypto - no longer used
  async getBTC(): Promise<void> {
    (await this.service.getBTC()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.BTC = keys
    })
  }
  async getETH(): Promise<void> {
    (await this.service.getETH()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.ETH = keys
    })
  }
  async getXRP(): Promise<void> {
    (await this.service.getXRP()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.XRP = keys
    })
  }
  async getLTC(): Promise<void> {
    (await this.service.getLTC()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.LTC = keys
    })
  }
  async getBCH(): Promise<void> {
    (await this.service.getBCH()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.BCH = keys
    })
  }
  async getETC(): Promise<void> {
    (await this.service.getETC()).subscribe((response: any) => {
      let keys = [];
      for (let key in response) {
        keys.push({ key: key, value: response[key] });
      }
      this.ETC = keys
    })
  }
}
