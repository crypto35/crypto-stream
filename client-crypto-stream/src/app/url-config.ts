import { Injectable } from '@angular/core';

@Injectable()
export class UrlConfig {
    // store url for our http calls
    public loginURL = "http://localhost:3000/login/clientLogin"
    public streamBTC = "http://localhost:3000/stream/BTC"
    public streamETH = "http://localhost:3000/stream/ETH"
    public streamXRP = "http://localhost:3000/stream/XRP"
    public streamLTC = "http://localhost:3000/stream/LTC"
    public streamBCH = "http://localhost:3000/stream/BCH"
    public streamETC = "http://localhost:3000/stream/ETC"
    public user = "http://localhost:3000/login/user"
}