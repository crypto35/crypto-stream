import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlConfig } from '../url-config';

@Injectable()
export class loginService {
    constructor(
        private http: HttpClient,
        private config: UrlConfig) { }

    // call to post new login
    // use service to store out url
    async getLogin(body: {}) {
        return await this.http.post(this.config.loginURL, body);
    }
}