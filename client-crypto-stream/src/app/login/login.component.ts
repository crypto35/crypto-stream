import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { loginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // simple login screen build to test and learn nestJS calls
  name: string = ''

  constructor(
    private router: Router,
    private service: loginService
  ) { }

  ngOnInit(): void {
  }

  // call login service to handel our http call
  async start(): Promise<void> {
    (await this.service.getLogin({name: this.name})).subscribe((login: any) => {
      if(login.login == true) return this.router.navigate(['stream'])
      if(login.login == false) return alert(login.msg)
      return alert('No response from server, try again')
    })
  }

}
