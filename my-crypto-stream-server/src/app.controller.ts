import { Controller, Get, Header } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('test') // path url expects 
  @Header('Content-Type', 'text/html') // can force the header, nestjs picks up automaticaly the content returned and sets the header for us
  getHello(): string {
    return this.appService.getHello();
  }
}

