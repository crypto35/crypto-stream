import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WsAdapter } from '@nestjs/platform-ws' //Add this line
import { SocketIoAdapter } from './socket-io.adapter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.useWebSocketAdapter(new WsAdapter(app)) // Add this line
  app.useWebSocketAdapter(new SocketIoAdapter(app, true));
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
