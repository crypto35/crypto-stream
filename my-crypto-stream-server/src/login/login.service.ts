import { Injectable } from "@nestjs/common";
import { Client } from "./login.model"

// decoratos imported from nestjs
@Injectable()
export class LoginService {
    // here we will "handel" our users
    // so we have array of users/clients
    // client model: how our clients should look like
    clients: Client[] = [];

    // here we handel our login
    newLogin(name: string){
        const newLogin = new Client(name);
        let date = new Date().toString()
        if(name == '' || name == null || name == undefined) return {login: false, msg: `provide name`}
        // we push our login name to clients
        this.clients.push(newLogin)
        return {login: true, msg: `${name}: loged in at ${date}`}
    }
}