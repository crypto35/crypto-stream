// create new controller to handel our login, login will now just be handeled in memory

// add post so we can use the post request
import { Controller, Post, Body, Get } from "@nestjs/common";
import { LoginService } from "./login.service";


// url: http://localhost:3000/login
@Controller('login')

// class that we are going to use to handel our "suppoused" login
export class LoginController {
    // inject our service
    // here we whant our instance of login
    // readonly makes it so that we never repolace loginService with a new value
    public user: string
    constructor(private readonly loginService: LoginService) {}
    
    // post call to login client side user, another login call can be when we 
    // have part of our client side embeded into another app and users from that site only whants to retrieve some of our data
    // there we can use a post call "apiLogin" to handel that request
    
    // we can reach the post body content by adding the @body decorator
    // 'name' field we are expecting
    // nestjs pars the incoming request and then in that object it will look for name property
    // and then provide that to us in the client argumant
    
    @Post('clientLogin')
    newLogin(@Body('name') client: string): any {
        // distrabute our code here we add login.service file
        // so we keep code clean and readable, we dont whant endless lines of code in one file
        this.user = client
        return this.loginService.newLogin(client)
    }
    @Get('user')
    userName(): string {
        return this.user
    }
    // postman test
    // url: http://localhost:3000/login/clientLogin
    // json body we send: {"name": "Daniel"}
    // reposne : {"login": true, "msg": "Daniel: loged in at Sat May 08 2021 17:28:54 GMT+0200 (South Africa Standard Time)"}
}
