// here we create the instance of client class
export class Client {
    // constructor so we can call new client
    // public accessor is shortcut so we dont have to add "this.id = id" and declear id: string above the constructor
    constructor(public name: string){ }
}