import { Module } from "@nestjs/common";
import { LoginController } from "./login.controller";
import { LoginService } from "./login.service";

@Module({
    controllers: [LoginController],
    providers: [LoginService]
})
export class loginModule {}

//set up our module for this login component