import { Module } from '@nestjs/common';
import { ApiStream } from './api-stream/api-stream.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { loginModule } from './login/login.module';

@Module({
  imports: [loginModule, ApiStream], // link the modules
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

// add our new controller and service to the file so they can get compiled when server starts, else our code will never reach them
// we do this by adding out module to imports to link them
