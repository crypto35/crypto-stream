import { HttpService, Injectable } from "@nestjs/common";

// decoratos imported from nestjs
@Injectable()
export class ApiStreamService {
    constructor(private httpService: HttpService) {}
    
    async ETH(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {ETH: x.data}
    }
    async BTC(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {BTC: x.data}
    }
    async XRP(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=XRP&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {XRP: x.data}
    }
    async LTC(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=LTC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {LTC: x.data}
    }
    async BCH(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=BCH&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {BCH: x.data}
    }
    async ETC(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/price?fsym=ETC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {ETC: x.data}
    }
    async multi(){
        let x =  await this.httpService
        .get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,LTC,BCH,ETC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
        .toPromise()
        return {ETC: x.data}
    }
    /*
    postman response
    {
    "ETC": {
        "Response": "Error",
        "Message": "You are over your rate limit please upgrade your account!",
        "HasWarning": false,
        "Type": 99,
        "RateLimit": {
            "calls_made": {
                "second": 1,
                "minute": 1,
                "hour": 3,
                "day": 12271,
                "month": 18978,
                "total_calls": 18978
            },
            "max_calls": {
                "second": 20,
                "minute": 300,
                "hour": 3000,
                "day": 7500,
                "month": 50000
            }
        },
        "Data": {}
    }
}
    */
}
