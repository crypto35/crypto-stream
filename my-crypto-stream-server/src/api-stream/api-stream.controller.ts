// create new controller to handel our login, login will now just be handeled in memory

// add post so we can use the post request
import { Controller, Post, Get, Body } from "@nestjs/common";
import { Observable } from "rxjs";
import { AppGateway } from "./api-stream.gateway";
import { ApiStreamService } from "./api-stream.service";

// url: http://localhost:3000/stream
@Controller('stream')
// class that we are going to use to handel our "suppoused" login
export class ApiStreamController {
    // inject our service
    constructor(
        private readonly apiStreamService: ApiStreamService,
        private gateway: AppGateway
    ) { }

    // postman test
    // url: http://localhost:3000/stream/btc
    // reposne : {"USD": 59018.78,"JPY": 6410237.42,"EUR": 48677.03}
    @Get('BTC')
    async BTC(): Promise<any> {
        return await this.apiStreamService.BTC()
    }
    @Get('ETH')
    async ETH(): Promise<any> {
        return await this.apiStreamService.ETH()
    }
    @Get('XRP')
    async XRP(): Promise<any> {
        return await this.apiStreamService.XRP()
    }
    @Get('LTC')
    async LTC(): Promise<any> {
        return await this.apiStreamService.LTC()
    }
    @Get('BCH')
    async BCH(): Promise<any> {
        return await this.apiStreamService.BCH()
    }
    @Get('ETC')
    async ETC(): Promise<any> {
        return await this.apiStreamService.ETC()
    }
    @Get('multi')
    async multi(): Promise<any> {
        return await this.apiStreamService.multi()
    }

}
