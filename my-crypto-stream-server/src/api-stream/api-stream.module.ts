import { HttpModule, Module } from "@nestjs/common";
import { ApiStreamController } from "./api-stream.controller";
import { AppGateway } from "./api-stream.gateway";
import { ApiStreamService } from "./api-stream.service";

@Module({
    imports: [HttpModule],
    controllers: [ApiStreamController],
    providers: [ApiStreamService, AppGateway]
})

export class ApiStream { 

}
