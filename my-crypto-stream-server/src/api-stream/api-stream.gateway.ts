import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WsResponse,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket } from 'socket.io';
import { ApiStreamService } from './api-stream.service';

@WebSocketGateway()
export class AppGateway
    implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private logger: Logger = new Logger('AppGateway');
    private value: any = {}

    constructor(private ss: ApiStreamService) { }

    @SubscribeMessage('msgToServe')
    async handleMessage(client: Socket, text: string): Promise<WsResponse<JSON>> {      
        // get stream data
        const promiseBTC = new Promise((resolve, reject) => {
            resolve(this.ss.BTC())
        });
        const promiseETH = new Promise((resolve, reject) => {
            resolve(this.ss.ETH())
        });
        const promiseXRP = new Promise((resolve, reject) => {
            resolve(this.ss.XRP())
        });
        const promiseLTC = new Promise((resolve, reject) => {
            resolve(this.ss.LTC())
        });
        const promiseBCH = new Promise((resolve, reject) => {
            resolve(this.ss.BCH())
        });
        const promiseETC = new Promise((resolve, reject) => {
            resolve(this.ss.ETC())
        });
        Promise.all([promiseBTC, promiseETH, promiseXRP, promiseLTC, promiseBCH, promiseETC]).then(values => {
            this.value = values
        });
        return {
            event: 'msgToClient',
            data: this.value
        }
    }
    afterInit(server: any): any {
        this.logger.log('init');
    }

    // handel on connection
    handleConnection(client: Socket, ...args: any[]): any {
        this.logger.log(`connected---${client.id}`);
    }

    //handel on disconnection
    handleDisconnect(client: Socket): any {
        this.logger.log(`disconnect---${client.id}`);
    }


}