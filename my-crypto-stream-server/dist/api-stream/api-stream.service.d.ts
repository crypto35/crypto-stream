import { HttpService } from "@nestjs/common";
export declare class ApiStreamService {
    private httpService;
    constructor(httpService: HttpService);
    ETH(): Promise<{
        ETH: any;
    }>;
    BTC(): Promise<{
        BTC: any;
    }>;
    XRP(): Promise<{
        XRP: any;
    }>;
    LTC(): Promise<{
        LTC: any;
    }>;
    BCH(): Promise<{
        BCH: any;
    }>;
    ETC(): Promise<{
        ETC: any;
    }>;
    multi(): Promise<{
        ETC: any;
    }>;
}
