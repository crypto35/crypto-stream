import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, WsResponse } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { ApiStreamService } from './api-stream.service';
export declare class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private ss;
    private logger;
    private value;
    constructor(ss: ApiStreamService);
    handleMessage(client: Socket, text: string): Promise<WsResponse<JSON>>;
    afterInit(server: any): any;
    handleConnection(client: Socket, ...args: any[]): any;
    handleDisconnect(client: Socket): any;
}
