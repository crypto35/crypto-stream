import { AppGateway } from "./api-stream.gateway";
import { ApiStreamService } from "./api-stream.service";
export declare class ApiStreamController {
    private readonly apiStreamService;
    private gateway;
    constructor(apiStreamService: ApiStreamService, gateway: AppGateway);
    BTC(): Promise<any>;
    ETH(): Promise<any>;
    XRP(): Promise<any>;
    LTC(): Promise<any>;
    BCH(): Promise<any>;
    ETC(): Promise<any>;
    multi(): Promise<any>;
}
