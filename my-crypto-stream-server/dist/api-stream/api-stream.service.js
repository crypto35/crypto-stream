"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiStreamService = void 0;
const common_1 = require("@nestjs/common");
let ApiStreamService = class ApiStreamService {
    constructor(httpService) {
        this.httpService = httpService;
    }
    async ETH() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { ETH: x.data };
    }
    async BTC() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { BTC: x.data };
    }
    async XRP() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=XRP&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { XRP: x.data };
    }
    async LTC() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=LTC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { LTC: x.data };
    }
    async BCH() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=BCH&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { BCH: x.data };
    }
    async ETC() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/price?fsym=ETC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { ETC: x.data };
    }
    async multi() {
        let x = await this.httpService
            .get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,LTC,BCH,ETC&tsyms=USD,GBP,EUR,JPY,ZAR&api_key={c98b9af80659d0519d4146300662a256d699a4662b0a772ba1da5df088573b9e}')
            .toPromise();
        return { ETC: x.data };
    }
};
ApiStreamService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [common_1.HttpService])
], ApiStreamService);
exports.ApiStreamService = ApiStreamService;
//# sourceMappingURL=api-stream.service.js.map