"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiStreamController = void 0;
const common_1 = require("@nestjs/common");
const api_stream_gateway_1 = require("./api-stream.gateway");
const api_stream_service_1 = require("./api-stream.service");
let ApiStreamController = class ApiStreamController {
    constructor(apiStreamService, gateway) {
        this.apiStreamService = apiStreamService;
        this.gateway = gateway;
    }
    async BTC() {
        return await this.apiStreamService.BTC();
    }
    async ETH() {
        return await this.apiStreamService.ETH();
    }
    async XRP() {
        return await this.apiStreamService.XRP();
    }
    async LTC() {
        return await this.apiStreamService.LTC();
    }
    async BCH() {
        return await this.apiStreamService.BCH();
    }
    async ETC() {
        return await this.apiStreamService.ETC();
    }
    async multi() {
        return await this.apiStreamService.multi();
    }
};
__decorate([
    common_1.Get('BTC'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "BTC", null);
__decorate([
    common_1.Get('ETH'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "ETH", null);
__decorate([
    common_1.Get('XRP'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "XRP", null);
__decorate([
    common_1.Get('LTC'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "LTC", null);
__decorate([
    common_1.Get('BCH'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "BCH", null);
__decorate([
    common_1.Get('ETC'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "ETC", null);
__decorate([
    common_1.Get('multi'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ApiStreamController.prototype, "multi", null);
ApiStreamController = __decorate([
    common_1.Controller('stream'),
    __metadata("design:paramtypes", [api_stream_service_1.ApiStreamService,
        api_stream_gateway_1.AppGateway])
], ApiStreamController);
exports.ApiStreamController = ApiStreamController;
//# sourceMappingURL=api-stream.controller.js.map