"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiStream = void 0;
const common_1 = require("@nestjs/common");
const api_stream_controller_1 = require("./api-stream.controller");
const api_stream_gateway_1 = require("./api-stream.gateway");
const api_stream_service_1 = require("./api-stream.service");
let ApiStream = class ApiStream {
};
ApiStream = __decorate([
    common_1.Module({
        imports: [common_1.HttpModule],
        controllers: [api_stream_controller_1.ApiStreamController],
        providers: [api_stream_service_1.ApiStreamService, api_stream_gateway_1.AppGateway]
    })
], ApiStream);
exports.ApiStream = ApiStream;
//# sourceMappingURL=api-stream.module.js.map