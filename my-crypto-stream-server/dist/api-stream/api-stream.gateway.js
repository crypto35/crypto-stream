"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const common_1 = require("@nestjs/common");
const socket_io_1 = require("socket.io");
const api_stream_service_1 = require("./api-stream.service");
let AppGateway = class AppGateway {
    constructor(ss) {
        this.ss = ss;
        this.logger = new common_1.Logger('AppGateway');
        this.value = {};
    }
    async handleMessage(client, text) {
        const promiseBTC = new Promise((resolve, reject) => {
            resolve(this.ss.BTC());
        });
        const promiseETH = new Promise((resolve, reject) => {
            resolve(this.ss.ETH());
        });
        const promiseXRP = new Promise((resolve, reject) => {
            resolve(this.ss.XRP());
        });
        const promiseLTC = new Promise((resolve, reject) => {
            resolve(this.ss.LTC());
        });
        const promiseBCH = new Promise((resolve, reject) => {
            resolve(this.ss.BCH());
        });
        const promiseETC = new Promise((resolve, reject) => {
            resolve(this.ss.ETC());
        });
        Promise.all([promiseBTC, promiseETH, promiseXRP, promiseLTC, promiseBCH, promiseETC]).then(values => {
            this.value = values;
        });
        return {
            event: 'msgToClient',
            data: this.value
        };
    }
    afterInit(server) {
        this.logger.log('init');
    }
    handleConnection(client, ...args) {
        this.logger.log(`connected---${client.id}`);
    }
    handleDisconnect(client) {
        this.logger.log(`disconnect---${client.id}`);
    }
};
__decorate([
    websockets_1.SubscribeMessage('msgToServe'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", Promise)
], AppGateway.prototype, "handleMessage", null);
AppGateway = __decorate([
    websockets_1.WebSocketGateway(),
    __metadata("design:paramtypes", [api_stream_service_1.ApiStreamService])
], AppGateway);
exports.AppGateway = AppGateway;
//# sourceMappingURL=api-stream.gateway.js.map