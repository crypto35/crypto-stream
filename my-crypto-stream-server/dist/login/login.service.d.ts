import { Client } from "./login.model";
export declare class LoginService {
    clients: Client[];
    newLogin(name: string): {
        login: boolean;
        msg: string;
    };
}
