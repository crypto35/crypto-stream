import { LoginService } from "./login.service";
export declare class LoginController {
    private readonly loginService;
    user: string;
    constructor(loginService: LoginService);
    newLogin(client: string): any;
    userName(): string;
}
